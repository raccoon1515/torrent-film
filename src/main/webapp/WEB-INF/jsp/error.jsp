<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Error</title>
</head>

<body>
    <h1>There's was error, message:</h1>
    <c:if test="${changeProxyButton}">
        <form method="post" action="/changeProxy">
            <button type="submit">Change proxy</button>
        </form>
    </c:if>

    <div>
        <c:forEach var="elem" items="${stackTrace}">
            <span>${elem.toString()}</span>
        </c:forEach>
    </div>

</body>

</html>