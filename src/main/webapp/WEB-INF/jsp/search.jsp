<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Search Results</title>
    <link rel="stylesheet" href="/css/search.css">
</head>

<body>
<h1>Search Results (Total ${countResults}):</h1>

<div id="result-table"
     data-html="<c:out value="${resultsTable}"/>"
></div>

<script src="/js/search.js"></script>
</body>

</html>