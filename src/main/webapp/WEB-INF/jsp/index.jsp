<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Torrent Film Downloader</title>
    <link rel="stylesheet" href="/css/index.css">
    <script src="/webjars/jquery/3.4.1/jquery.min.js"></script>
    <script src="/webjars/sockjs-client/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/stomp.min.js"></script>
    <script src="/js/index.js"></script>
</head>

<body>
    <h1>Hello, Enter the film name:</h1>

    <div>
        <form method="GET" action="/search">
            <label for="filmName" title="Film name">
                <input id="filmName" name="film" placeholder="Film name" type="text" autofocus/>
            </label>
            <button type="submit">Search</button>
        </form>
    </div>

    <div><h1>Download List:</h1></div>
    <div id="downloadList">
        <div class="download-item">
            <div class="download-status-value">File name</div>
            <div class="download-status-value">Completed</div>
            <div class="download-status-value">Downloaded this session</div>
            <div class="download-status-value">Connected Peers</div>
        </div>
        <c:forEach var="download" items="${downloadList}">
            <div id="torrentStatus-${download}" class="download-item">
                <a href="/download?torrent=${download}"><div class="download-status-value torrentName"></div></a>
                <div class="download-status-value downloadedPercent"></div>
                <div class="download-status-value downloadedBytes"></div>
                <div class="download-status-value peersConnected"></div>
                <span hidden class="torrentIdHidden">${download}</span>
            </div>
        </c:forEach>
    </div>

</body>

</html>