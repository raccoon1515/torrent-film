<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Download process</title>
    <script src="/webjars/sockjs-client/sockjs.min.js"></script>
    <script src="/webjars/stomp-websocket/stomp.min.js"></script>
    <script src="/webjars/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
<h1>Download process:</h1>
<div>
    <div>Torrent name:
        <span id="torrentName"></span>
    </div>
    <div>Completed:
        <span id="downloadedPercent"></span>
    </div>
    <div>Current session downloaded:
        <span id="downloadedBytes"></span>
    </div>
    <div>Peers connected:
        <span id="peersConnected"></span>
    </div>
</div>

<script src="/js/download.js"></script>
</body>

</html>