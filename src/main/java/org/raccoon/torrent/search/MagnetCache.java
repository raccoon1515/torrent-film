package org.raccoon.torrent.search;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class MagnetCache {
    private final Cache<Integer, MagnetLink> cache;

    public MagnetCache() {
        cache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .maximumSize(100)
                .build();
    }

    @Nullable
    MagnetLink getIfPresent(@NotNull final Integer torrentId) {
        return cache.getIfPresent(torrentId);
    }

    void put(@NotNull final Integer torrentId, @NotNull final MagnetLink magnetLink) {
        cache.put(torrentId, magnetLink);
    }

}
