package org.raccoon.torrent.search.proxy.converter;

import org.raccoon.torrent.exception.PropertiesException;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.net.Proxy;

@Component
@ConfigurationPropertiesBinding
public class ProxyTypeConverter implements Converter<String, Proxy.Type> {

    @Override
    public Proxy.Type convert(String source) {
        if (source.equalsIgnoreCase("http")) {
            return Proxy.Type.HTTP;
        } else if (source.equalsIgnoreCase("socks")) {
            return Proxy.Type.SOCKS;
        } else {
            throw new PropertiesException("Failed to convert proxy type");
        }
    }
}
