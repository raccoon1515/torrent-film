package org.raccoon.torrent.search.proxy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class ProxyProvider {
    private static final String PROXY_FILE_NAME = "proxies.list";
    private static final Pattern pattern = Pattern.compile("(?<host>(\\d{1,3}\\.){3}\\d{1,3})(:(?<port>\\d{1,5}))?");
    private static final String GITHUB_PROXIES_URL =
            "https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list-raw.txt";

    private final List<Proxy> proxyList = new ArrayList<>();

    @Value("${torrent.proxy.default.port}")
    private Integer defaultPort;
    @Value("${torrent.proxy.type}")
    private Proxy.Type proxyType;
    @Value("${torrent.proxy.auth.username}")
    private String authUser;
    @Value("${torrent.proxy.auth.password}")
    private String authPass;

    public void changeProxy() {
        this.getProxy();
    }

    @Nullable
    public Proxy getProxy() {
        return proxyList.stream().findFirst().orElse(null);
    }

    public void invalidateProxy(final Proxy proxy) {
        proxyList.remove(proxy);
    }

    @PostConstruct
    private void fillProxyList() throws IOException {
        loadProxiesFile();
        loadGithubProxy();

        if (authUser != null && authPass != null) {
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(authUser, authPass.toCharArray());
                }
            });
        }
    }

    private void loadProxiesFile() throws IOException {
        final InputStream proxiesListStream = getClass()
                .getClassLoader()
                .getResourceAsStream(PROXY_FILE_NAME);

        if (proxiesListStream == null) {
            log.warn("File '" + PROXY_FILE_NAME + "' not found");
            return;
        }

        final BufferedReader reader = new BufferedReader(new InputStreamReader(proxiesListStream));
        reader.lines().forEach(this::addProxy);
        reader.close();
    }

    private void loadGithubProxy() throws IOException {
        final URL url = new URL(GITHUB_PROXIES_URL);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        reader.lines().forEach(this::addProxy);
        reader.close();
    }

    private void addProxy(final String proxyLine) {
        final Matcher matcher = pattern.matcher(proxyLine);
        if (matcher.matches()) {
            final String host = matcher.group("host");
            final String port = ObjectUtils.defaultIfNull(matcher.group("port"), String.valueOf(defaultPort));

            final InetSocketAddress socketAddress = new InetSocketAddress(host, Integer.parseInt(port));
            final Proxy proxy = new Proxy(proxyType, socketAddress);
            proxyList.add(proxy);
        }
    }

}
