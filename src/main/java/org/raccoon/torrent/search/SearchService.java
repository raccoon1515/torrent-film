package org.raccoon.torrent.search;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection.KeyVal;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.raccoon.torrent.CommonCons;
import org.raccoon.torrent.exception.SearchException;
import org.raccoon.torrent.search.proxy.ProxyProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.Proxy;
import java.util.*;

@Service
public class SearchService {

    @Value("${torrent.rutracker.cookie}")
    private String sessionCookie;

    private final ProxyProvider proxyProvider;
    private final List<Connection.KeyVal> commonSearchParams;
    private final MagnetCache magnetCache;


    public SearchService(ProxyProvider proxyProvider, MagnetCache magnetCache) {
        this.proxyProvider = proxyProvider;
        this.magnetCache = magnetCache;

        commonSearchParams = new ArrayList<>();
        commonSearchParams.add(KeyVal.create("f[]", "187"));
        commonSearchParams.add(KeyVal.create("f[]", "572"));
        commonSearchParams.add(KeyVal.create("f[]", "2090"));
        commonSearchParams.add(KeyVal.create("f[]", "2221"));
        commonSearchParams.add(KeyVal.create("f[]", "2091"));
        commonSearchParams.add(KeyVal.create("f[]", "2092"));
        commonSearchParams.add(KeyVal.create("f[]", "2200"));
        commonSearchParams.add(KeyVal.create("f[]", "1950"));
        commonSearchParams.add(KeyVal.create("o", "10"));
        commonSearchParams.add(KeyVal.create("s", "2"));
        commonSearchParams.add(KeyVal.create("oop", "1"));
        commonSearchParams.add(KeyVal.create("pn", ""));
    }

    public SearchResult searchFilm(@NotNull final String filmName) {
        final String url = CommonCons.FILM_SEARCH_URL_PREFIX + filmName;
        final List<Connection.KeyVal> searchParams = new ArrayList<>(commonSearchParams);
        searchParams.add(KeyVal.create("nm", filmName));

        final Document document = executeRequest(url, Connection.Method.POST, searchParams);
        return parseSearchResults(document);
    }

    public MagnetLink getMagnetLink(@NotNull final Integer torrentId) {
        MagnetLink magnetLink = magnetCache.getIfPresent(torrentId);
        if (magnetLink == null) {
            final String url = CommonCons.TORRENT_FILE_URL_PREFIX + torrentId;
            final Document document = executeRequest(url, Connection.Method.GET, null);
            magnetLink = parseMagnetLink(document, torrentId);
            magnetCache.put(torrentId, magnetLink);
        }

        return magnetLink;
    }

    private Document executeRequest(final String url,
                                    final Connection.Method method,
                                    @Nullable final Collection<Connection.KeyVal> data) {

        final Connection connection = getConnection(url);
        connection.method(method);
        if (data != null) {
            connection.data(data);
        }

        Proxy proxy;
        while ((proxy = proxyProvider.getProxy()) != null) {
            connection.proxy(proxy);
            try {
                return connection.execute().parse();
            } catch (IOException e) {
                proxyProvider.invalidateProxy(proxy);
            }
        }

        throw new SearchException("All proxies are invalid");
    }

    private SearchResult parseSearchResults(final Document document) {
        final Elements elements = document.select("p:contains(Результатов поиска:)");
        final int totalResults = Integer.parseInt(elements.first().text().split(" ")[2]);

        final Element resultsTable = document.getElementById("tor-tbl");
        return new SearchResult(resultsTable.outerHtml(), totalResults);
    }

    private MagnetLink parseMagnetLink(final Document document,
                                       Integer torrentId) {
        final Element linkNode = document.getElementById("tor-hash");
        final String magnetHash = linkNode.text();
        final String magnetLink = linkNode.parent().attr("href");
        return new MagnetLink(magnetLink, magnetHash, torrentId);
    }

    private Connection getConnection(final String url) {
        return Jsoup.connect(url)
                .header("Content-Language", "ru-RU")
                .cookie(CommonCons.SESSION_KEY, sessionCookie);
    }

}
