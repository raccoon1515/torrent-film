package org.raccoon.torrent.search;

public class MagnetLink {
    private String magnetLink;
    private String magnetHash;
    private Integer torrentId;

    MagnetLink(String magnetLink, String magnetHash, Integer torrentId) {
        this.magnetLink = magnetLink;
        this.magnetHash = magnetHash;
        this.torrentId = torrentId;
    }

    public String getMagnetLink() {
        return magnetLink;
    }

    public Integer getTorrentId() {
        return torrentId;
    }

    public String getMagnetHash() {
        return magnetHash;
    }

}
