package org.raccoon.torrent.search;

public class SearchResult {
    private final String resultsTableHtml;
    private final int totalResults;

    SearchResult(String resultsTableHtml, int totalResults) {
        this.resultsTableHtml = resultsTableHtml;
        this.totalResults = totalResults;
    }

    public String getResultsTableHtml() {
        return resultsTableHtml;
    }

    public int getTotalResults() {
        return totalResults;
    }

}
