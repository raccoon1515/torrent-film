package org.raccoon.torrent.controller;

import org.raccoon.torrent.ViewCons;
import org.raccoon.torrent.dowloader.DownloadCache;
import org.raccoon.torrent.search.SearchResult;
import org.raccoon.torrent.search.SearchService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    private final SearchService searchService;
    private final DownloadCache downloadCache;

    public IndexController(SearchService searchService, DownloadCache downloadCache) {
        this.searchService = searchService;
        this.downloadCache = downloadCache;
    }

    @GetMapping(value = {"/", "/index"})
    public ModelAndView getIndex() {
        final ModelAndView view = new ModelAndView(ViewCons.INDEX);
        view.addObject("downloadList", downloadCache.getAll().keySet());
        return view;
    }

    @GetMapping("/search")
    public ModelAndView searchFilm(@RequestParam("film") String filmName) {
        final ModelAndView view = new ModelAndView(ViewCons.SEARCH);
        final SearchResult searchResult = searchService.searchFilm(filmName);
        view.addObject("resultsTable", searchResult.getResultsTableHtml());
        view.addObject("countResults", searchResult.getTotalResults());
        return view;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView error(final Exception exception) {
        final ModelAndView view = new ModelAndView(ViewCons.ERROR);
        view.addObject("stackTrace", exception.getStackTrace());
        view.addObject("changeProxyButton", true);
        return view;
    }

}
