package org.raccoon.torrent.controller;

import lombok.extern.slf4j.Slf4j;
import org.raccoon.torrent.ViewCons;
import org.raccoon.torrent.dowloader.DownloadService;
import org.raccoon.torrent.search.MagnetLink;
import org.raccoon.torrent.search.SearchService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/download")
public class DownloadController {

    private final SearchService searchService;
    private final DownloadService downloadService;

    @Value("${torrent.rutracker.cookie}")
    private String sessionCookie;

    public DownloadController(SearchService searchService,
                              DownloadService downloadService) {
        this.searchService = searchService;
        this.downloadService = downloadService;
    }

    @GetMapping
    public ModelAndView startDownload(@RequestParam("torrent") Integer torrentId) {
        final MagnetLink magnetLink = searchService.getMagnetLink(torrentId);
        downloadService.startDownload(magnetLink);
        return new ModelAndView(ViewCons.DOWNLOAD);
    }

}
