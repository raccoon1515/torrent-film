package org.raccoon.torrent.controller;

import lombok.extern.slf4j.Slf4j;
import org.raccoon.torrent.ViewCons;
import org.raccoon.torrent.search.proxy.ProxyProvider;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
public class ProxyController {

    private final ProxyProvider proxyProvider;

    public ProxyController(ProxyProvider proxyProvider) {
        this.proxyProvider = proxyProvider;
    }

    @PostMapping("/changeProxy")
    public ModelAndView changeProxy() {
        log.info("Requested changing proxy");
        proxyProvider.changeProxy();
        return new ModelAndView(ViewCons.REDIRECT_INDEX);
    }

}
