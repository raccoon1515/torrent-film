package org.raccoon.torrent.exception;

public class DownloadException extends RuntimeException {

    public DownloadException(String message) {
        super(message);
    }
}
