package org.raccoon.torrent.exception;

public class SearchException extends RuntimeException {
    public SearchException(String message, Throwable e) {
        super(message, e);
    }

    public SearchException(String message) {
        super(message);
    }

}
