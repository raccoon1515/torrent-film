package org.raccoon.torrent.dowloader;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

@Component
public class DownloadCache {

    private final Cache<Integer, DownloadEntity> cache;

    public DownloadCache() {
        cache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.DAYS)
                .maximumSize(100)
                .build();
    }

    @Nullable DownloadEntity getIfPresent(@NotNull final Integer torrentId) {
        return cache.getIfPresent(torrentId);
    }

    public void invalidate(@NotNull final Integer torrentId) {
        cache.invalidate(torrentId);
    }

    @NotNull
    public ConcurrentMap<Integer, DownloadEntity> getAll() {
        return cache.asMap();
    }

    void put(@NotNull final Integer torrentId, @NotNull final DownloadEntity downloadEntity) {
        cache.put(torrentId, downloadEntity);
    }

}
