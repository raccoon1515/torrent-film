package org.raccoon.torrent.dowloader;

import bt.Bt;
import bt.data.Storage;
import bt.data.file.FileSystemStorage;
import bt.dht.DHTConfig;
import bt.dht.DHTModule;
import bt.runtime.BtClient;
import bt.runtime.BtRuntime;
import bt.runtime.Config;
import bt.torrent.TorrentSessionState;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.raccoon.torrent.JavaSecurityConfig;
import org.raccoon.torrent.search.MagnetLink;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Slf4j
@Service
public class DownloadService {

    private static final String SOCKET_DESTINATION = "/topic/downloadStatus";

    private final SimpMessagingTemplate messagingTemplate;
    private final DownloadCache downloadCache;

    private Storage storage;
    private BtRuntime runtime;
    @Value("${torrent.download.path}")
    private String targetDir;
    @Value("${torrent.download.custom.host}")
    private String customHost;

    public DownloadService(SimpMessagingTemplate messagingTemplate,
                           DownloadCache downloadCache) {
        this.messagingTemplate = messagingTemplate;
        this.downloadCache = downloadCache;
    }

    public void startDownload(@NotNull final MagnetLink magnetLink) {
        final DownloadEntity downloadEntity = downloadCache.getIfPresent(magnetLink.getTorrentId());
        if (downloadEntity == null) {
            runDownload(magnetLink);
        }
    }

    private void runDownload(final MagnetLink magnetLink) {
        final String destination = SOCKET_DESTINATION + "/" + magnetLink.getTorrentId();
        final BtClient client = Bt.client(runtime)
                .storage(storage)
                .magnet(magnetLink.getMagnetLink())
                .afterTorrentFetched(torrent -> {
                    log.info(torrent.getName());
                    messagingTemplate.convertAndSend(destination,
                            DownloadStatus.getInstance(torrent.getName()));
                })
                .stopWhenDownloaded()
                .sequentialSelector()
                .build();

        // launch
        // todo: exception handle
        client.startAsync(newListener(magnetLink), 2000);

        downloadCache.put(magnetLink.getTorrentId(), new DownloadEntity(client));
    }

    private Consumer<TorrentSessionState> newListener(final MagnetLink magnetLink) {
        final String destination = SOCKET_DESTINATION + "/" + magnetLink.getTorrentId();
        return torrentSessionState ->
                messagingTemplate.convertAndSend(destination,
                        DownloadStatus.getInstance(torrentSessionState));
    }

    @PostConstruct
    private void init() {
        JavaSecurityConfig.configureSecurity();

        final Config config = new Config() {
            @Override
            public int getNumOfHashingThreads() {
                return Runtime.getRuntime().availableProcessors() * 2;
            }

//            @Override
//            public InetAddress getAcceptorAddress() {
//                if (customHost != null) {
//                    try {
//                        return InetAddress.getByName(customHost);
//                    } catch (UnknownHostException e) {
//                        log.error(e.getMessage());
//                    }
//                }
//                return super.getAcceptorAddress();
//            }
        };
        // enable bootstrapping from public routers
        final DHTModule dhtModule = new DHTModule(new DHTConfig() {
            @Override
            public boolean shouldUseRouterBootstrap() {
                return true;
            }
        });

        runtime = BtRuntime.builder(config)
                .autoLoadModules()
                .module(dhtModule)
                .disableAutomaticShutdown()
                .build();

        // get download directory
        final String path = new String(targetDir.getBytes(StandardCharsets.UTF_8));
        final Path targetDirectory = new File(path).toPath();
        // create file system based backend for torrent data
        storage = new FileSystemStorage(targetDirectory);
    }

}
