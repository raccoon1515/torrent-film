package org.raccoon.torrent.dowloader;

import bt.runtime.BtClient;

class DownloadEntity {
    private final BtClient client;

    DownloadEntity(BtClient client) {
        this.client = client;
    }

    BtClient getClient() {
        return client;
    }

}
