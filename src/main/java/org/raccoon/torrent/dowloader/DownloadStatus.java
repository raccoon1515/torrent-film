package org.raccoon.torrent.dowloader;

import bt.torrent.TorrentSessionState;

public class DownloadStatus {
    private static DownloadStatus instance = new DownloadStatus();

    private String torrentName;
    private int peersConnected;
    private int percentCompleted;
    private long downloaded;

    private DownloadStatus() {
    }

    static DownloadStatus getInstance(final TorrentSessionState sessionState) {
        instance.percentCompleted = (int) (((double) sessionState.getPiecesComplete()) / sessionState.getPiecesTotal() * 100);
        instance.peersConnected = sessionState.getConnectedPeers().size();
        instance.downloaded = sessionState.getDownloaded();
        return instance;
    }

    static DownloadStatus getInstance(final String torrentName) {
        instance.percentCompleted = 0;
        instance.peersConnected = 0;
        instance.downloaded = 0;
        instance.torrentName = torrentName;
        return instance;
    }

    public String getTorrentName() {
        return torrentName;
    }

    public int getPeersConnected() {
        return peersConnected;
    }

    public double getPercentCompleted() {
        return percentCompleted;
    }

    public long getDownloaded() {
        return downloaded;
    }

    @Override
    public String toString() {
        return "DownloadStatus{" +
                "torrentName='" + torrentName + '\'' +
                ", peersConnected=" + peersConnected +
                ", percentCompleted=" + percentCompleted +
                ", downloaded=" + downloaded +
                '}';
    }

}
