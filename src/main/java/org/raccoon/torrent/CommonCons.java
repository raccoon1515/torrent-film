package org.raccoon.torrent;

public final class CommonCons {
    public static final String SESSION_KEY = "bb_session";
    public static final String TORRENT_FILE_URL_PREFIX = "https://rutracker.org/forum/viewtopic.php?t=";
    public static final String FILM_SEARCH_URL_PREFIX =
            "https://rutracker.org/forum/tracker.php?f=187,572,1950,2090,2091,2092,2093,2200,2221&nm=";
}
