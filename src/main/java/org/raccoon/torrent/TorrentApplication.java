package org.raccoon.torrent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorrentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TorrentApplication.class, args);
    }

}
