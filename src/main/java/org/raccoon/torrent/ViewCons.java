package org.raccoon.torrent;

public final class ViewCons {
    public static final String INDEX = "index";
    public static final String ERROR = "error";
    public static final String SEARCH = "search";
    public static final String DOWNLOAD = "download";

    public static final String REDIRECT_PREFIX = "redirect:";
    public static final String REDIRECT_INDEX = REDIRECT_PREFIX + INDEX;
    public static final String REDIRECT_ERROR = REDIRECT_PREFIX + ERROR;
    public static final String REDIRECT_SEARCH = REDIRECT_PREFIX + SEARCH;
    public static final String REDIRECT_DOWNLOAD = REDIRECT_PREFIX + DOWNLOAD;
}
