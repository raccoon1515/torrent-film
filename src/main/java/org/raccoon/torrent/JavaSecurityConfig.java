package org.raccoon.torrent;

import lombok.extern.slf4j.Slf4j;

import java.security.Security;

@Slf4j
public final class JavaSecurityConfig {
    /**
     * @see <a href="http://www.oracle.com/technetwork/java/javase/8u152-relnotes-3850503.html">this link</a>
     */
    public static void configureSecurity() {
        String key = "crypto.policy";
        String value = "unlimited";
        try {
            Security.setProperty(key, value);
        } catch (Exception e) {
            log.error(String.format("Failed to set security property '%s' to '%s'", key, value), e);
        }
    }

}
