let stompClient = null;
const torrentId = new URLSearchParams(document.location.search.substring(1))
    .get('torrent');
const torrentNameElem = document.getElementById('torrentName');
const downloadedPercent = document.getElementById('downloadedPercent');
const downloadedBytes = document.getElementById('downloadedBytes');
const peersConnectedElem = document.getElementById('peersConnected');

const setConnected = connected => {
    console.log('Connected = ' + connected);
};

const setDisplayValue = (elem, value) => {
    elem.innerText = value;
};

const showStatus = status => {
    const {downloaded, torrentName, percentCompleted, peersConnected} = status;
    setDisplayValue(torrentNameElem, torrentName);
    setDisplayValue(downloadedPercent, percentCompleted + ' %');
    setDisplayValue(downloadedBytes, formatBytes(downloaded));
    setDisplayValue(peersConnectedElem, peersConnected);
};

const connect = () => {
    const socket = new SockJS('/downloadStatus');
    stompClient = Stomp.over(socket);
    stompClient.debug = null;
    stompClient.connect({}, frame => {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/downloadStatus/' + torrentId, status => {
            showStatus(JSON.parse(status.body));
        });
    });
};

const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};


const sendRequest = (url, callback) => {
    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        data: {torrentId: torrentId},
        success: callback,
        error: (error) => {
            console.log(error);
        }
    })
};

connect();
