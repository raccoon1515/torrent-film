document.addEventListener('DOMContentLoaded', () => {
    insertSearchResultTable();
    replaceTableDownloadLinks();
});

const insertSearchResultTable = () => {
    const HTML_ATTR_NAME = 'data-html';
    const resultTable = document.getElementById('result-table');
    resultTable.innerHTML = '<table>' + resultTable.getAttribute(HTML_ATTR_NAME) + '</table>';
    resultTable.removeAttribute(HTML_ATTR_NAME);

};

const replaceTableDownloadLinks = () => {
    const torrentLinks = document.getElementsByClassName('wbr t-title');
    for (let i = 0; i < torrentLinks.length; i++) {
        const link = torrentLinks[i].lastElementChild;
        const href = link.getAttribute('href');
        if (href) {
            const newHref = href.replace('viewtopic.php?t=', '/download?torrent=');
            link.setAttribute('href', newHref);
        }
    }

    // remove other links from table
    const resultTable = document.getElementById('result-table');
    const allTableLinks = resultTable.querySelectorAll('a');
    for (let i = 0; i < allTableLinks.length; i++) {
        const link = allTableLinks[i];
        const href = link.getAttribute('href');
        if (href.includes('dl.php?t=') || href.includes('tracker.php?')) {
            link.removeAttribute('href');
        }
    }
};
