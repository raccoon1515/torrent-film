let stompClient = null;

const setConnected = connected => {
    console.log('Connected = ' + connected);
};

const setDisplayValue = ($elem, status) => {
    const {downloaded, torrentName, percentCompleted, peersConnected} = status;
    $elem.find('.torrentName').text(torrentName);
    $elem.children('.downloadedPercent').text(percentCompleted + ' %');
    $elem.children('.downloadedBytes').text(formatBytes(downloaded));
    $elem.children('.peersConnected').text(peersConnected);
};

const showStatus = (status, torrentId) => {
    const $statusElem = $('#torrentStatus-' + torrentId);
    setDisplayValue($statusElem, status);
};

const connect = () => {
    const socket = new SockJS('/downloadStatus');
    stompClient = Stomp.over(socket);
    stompClient.debug = null;
    stompClient.connect({}, frame => {
        setConnected(true);
        const torrentIds = $(".torrentIdHidden");
        for (let torrentIdElem of torrentIds) {
            const torrentId = $(torrentIdElem).text();
            stompClient.subscribe('/topic/downloadStatus/' + torrentId, (status) => {
                showStatus(JSON.parse(status.body), torrentId);
            });
        }
    });
};

const formatBytes = (bytes, decimals = 2) => {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};


$(document).ready(() => {
        connect();
});
